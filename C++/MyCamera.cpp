// Fill out your copyright notice in the Description page of Project Settings.

#include "MyCamera.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/UMG/Public/Components/SlateWrapperTypes.h"
#include "Runtime/Core/Public/Internationalization/Text.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Blueprint/UserWidget.h"

static void vectorChangeForGrid(FVector* vector);
static void vectorChangeForArrayOfMove(FVector* vector);
static void vectorChangeFromGrid(FVector* vector);

// Sets default values
AMyCamera::AMyCamera()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	nameOfCube = nullptr;

	Money = 200;

	forMenu = false;

	onFactory = false;

	PlayerStartLoc = FVector(0, 0, 0);

	AutoPossessPlayer = EAutoReceiveInput::Player0;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	UCameraComponent* OurCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("OurCamera"));
	OurCamera->SetupAttachment(RootComponent);
	OurCamera->SetRelativeLocation(FVector(-100.0f, 0.0f, 350.0f));
	OurCamera->SetRelativeRotation(FRotator(-60.0f, 0.0f, 0.0f));
	MaxSpeed = 500;

	static ConstructorHelpers::FObjectFinder<UMaterial> WorldGrid(TEXT("/Game/MyContent/WorldGridMaterialWithRectagles.WorldGridMaterialWithRectagles"));
	{
		MyMaterial = WorldGrid.Object;
	}
	MyDynamicMaterialInst = UMaterialInstanceDynamic::Create(MyMaterial, OurCamera);

	OurCamera->AddOrUpdateBlendable(MyDynamicMaterialInst);

	static ConstructorHelpers::FClassFinder<UGameInterface> YourBlueprintName(TEXT("WidgetBlueprintGeneratedClass'/Game/MyContent/UI/BP_GameInterface.BP_GameInterface_C'"));
	{
		wGameInterface = YourBlueprintName.Class;
	}

	static ConstructorHelpers::FClassFinder<UUserWidget> YourBlueprintNameForPause(TEXT("WidgetBlueprintGeneratedClass'/Game/MyContent/UI/BP_PauseMenu.BP_PauseMenu_C'"));
	{
		wPauseMenu = YourBlueprintNameForPause.Class;
	}
}

// Called when the game starts or when spawned
void AMyCamera::BeginPlay()
{
	Super::BeginPlay();

	APlayerController * controller = GetWorld()->GetFirstPlayerController();
	////////////////
	MyPauseMenu = CreateWidget<UUserWidget>(controller, wPauseMenu);
	if (MyPauseMenu)
	{
		//MyPauseMenu->AddToViewport(1);
	}
	////////////////
	MyGameInterface = CreateWidget<UGameInterface>(controller, wGameInterface);
	if (MyGameInterface)
	{
		MyGameInterface->AddToViewport();
	}
	MyGameInterface->myCamera = this;
	///////////////
	FInputModeGameAndUI inp;
	inp.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
	if (MyGameInterface != nullptr && MyPauseMenu != nullptr) {
		inp.SetWidgetToFocus(MyGameInterface->TakeWidget());
		inp.SetWidgetToFocus(MyPauseMenu->TakeWidget());
	}
	controller->SetInputMode(inp);

	MyGameInterface->AttackButton->SetVisibility(ESlateVisibility::Collapsed);
	MyGameInterface->MoveButton->SetVisibility(ESlateVisibility::Collapsed);
	MyGameInterface->TankProductionButton->SetVisibility(ESlateVisibility::Collapsed);
	MyGameInterface->MoneyTextBlock->SetText(FText::FromString(FString::FromInt(Money)));
	MyGameInterface->HorizontalBoxForTank->SetVisibility(ESlateVisibility::Collapsed);
	MyGameInterface->Image_YouDied->SetVisibility(ESlateVisibility::Collapsed);
	MyGameInterface->Button_ToMainMenu->SetVisibility(ESlateVisibility::Collapsed);

	////////////////////////////////
	nameOfLevel = UGameplayStatics::GetCurrentLevelName(this);
	if (nameOfLevel == FString("MyWorld")) {
		PlayerStartLoc = FVector(50, -107, 20);
		world[7][7].empty = false;
		world[12][12].empty = false;
		world[4][15].empty = false;
		world[15][4].empty = false;
	}
	else if (nameOfLevel == FString("MyWorld_2")) {
		PlayerStartLoc = FVector(50, -107, 20);
		world[4][2].empty = false;
		world[9][2].empty = false;
		world[14][2].empty = false;
	}
	else if (nameOfLevel == FString("MyWorld_3")) {
		PlayerStartLoc = FVector(50, -107, 20);
		world[4][2].empty = false;
		world[5][7].empty = false;
	}
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	myFactory = GetWorld()->SpawnActor<AMyFactory>(PlayerStartLoc, FRotator(0, 90, 0), SpawnInfo);
	myFactory->team = Team::VE_Player;
	myFactory->myCamera = this;

}

// Called every frame
void AMyCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorLocalOffset(ConsumeMovementInputVector() * DeltaTime);

	//UGameplayStatics::GetPlayerController(GetWorld(), 0);
	APlayerController * controller = GetWorld()->GetFirstPlayerController();

	FHitResult hit = FHitResult(ForceInit);
	bool bMouseResult = controller->GetHitResultUnderCursor(ECC_WorldStatic, true, hit);
	auto a = hit.GetActor();
	if (a != nullptr) {
		FLinearColor colorVector = FLinearColor();
		if (a->GetName() == "Floor") {
			FVector vector = hit.Location;
			vectorChangeForGrid(&vector);
			colorVector = vector;
			MyDynamicMaterialInst->SetVectorParameterValue("SelectedPos", colorVector);
		}
		else if (a->IsA(AMyPawn::StaticClass()) || a->IsA(ASquareForMove::StaticClass()) || a->IsA(APawnForAttack::StaticClass())) {
			FVector vector = a->GetActorLocation();
			vectorChangeForGrid(&vector);
			colorVector = vector;
			MyDynamicMaterialInst->SetVectorParameterValue("SelectedPos", colorVector);
		}
	}
}

// Called to bind functionality to input
void AMyCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &AMyCamera::MoveForward).bConsumeInput = false;
	PlayerInputComponent->BindAxis("MoveRight", this, &AMyCamera::MoveRight).bConsumeInput = false;
	PlayerInputComponent->BindAxis("MouseButtons", this, &AMyCamera::MouseButtons).bConsumeInput = false;
	PlayerInputComponent->BindAxis("Pause", this, &AMyCamera::Pause).bConsumeInput = false;

}

void AMyCamera::MoveForward(float value)
{	
	if ((value > 0 && this->GetActorLocation().X < MyMain->size_x * 100) || (value < 0 && this->GetActorLocation().X > -200)) {
		AddMovementInput(GetActorForwardVector(), value * MaxSpeed);
	}
}

void AMyCamera::MoveRight(float value)
{
	if ((value > 0 && this->GetActorLocation().Y < MyMain->size_y * 100) || (value < 0 && this->GetActorLocation().Y > -200)) {
		AddMovementInput(GetActorRightVector(), value * MaxSpeed);
	}
}

void AMyCamera::Pause(float value)
{
	APlayerController * controller = GetWorld()->GetFirstPlayerController();
	if (value == 1) {
		if (forMenu == false) {
			if (MyPauseMenu != nullptr) {
				MyPauseMenu->AddToViewport(1);
			}
			if (controller != nullptr)
			{
				controller->SetPause(true);
			}
			forMenu = true;
		}
		else {
			if (controller != nullptr)
			{
				controller->SetPause(false);
			}
			if (MyPauseMenu != nullptr) {
				MyPauseMenu->RemoveFromViewport();
			}
			forMenu = false;
		}
	}
}

void AMyCamera::MouseButtons(float value) {
	if (value == 1) {
		if (nameOfCube != nullptr) {
			///////////////////////
			APlayerController * controller = GetWorld()->GetFirstPlayerController();

			FHitResult hit = FHitResult(ForceInit);
			bool bMouseResult = controller->GetHitResultUnderCursor(ECC_WorldStatic, true, hit);
			auto hitActor = hit.GetActor();
			if (hitActor != nullptr) {
				if (hitActor->GetName() == "Floor") {
					for (int i = nameOfCube->arrayForMove.Num() - 1; i >= 0; i--) {
						if (nameOfCube->arrayForMove[i] != nullptr) nameOfCube->arrayForMove[i]->Destroy();
						nameOfCube->arrayForMove.RemoveAt(i);
					}
					while (nameOfCube->__road.size() != 0) {
						nameOfCube->__road.pop_front();
					}
					for (int i = nameOfCube->arrayForAttack.Num() - 1; i >= 0; i--) {
						if (nameOfCube->arrayForAttack[i] != nullptr) nameOfCube->arrayForAttack[i]->Destroy();
						nameOfCube->arrayForAttack.RemoveAt(i);
					}
					nameOfCube->MyDynamicMaterialInst->SetScalarParameterValue("ColorAlpha", 0);
					MyGameInterface->AttackButton->SetVisibility(ESlateVisibility::Collapsed);
					MyGameInterface->MoveButton->SetVisibility(ESlateVisibility::Collapsed);
					MyGameInterface->HorizontalBoxForTank->SetVisibility(ESlateVisibility::Collapsed);
					nameOfCube = nullptr;
				}
			}
			///////////////////////
			/*if (nameOfCube->arrayForMove.Num() != 0) {
			bool includ = false;
			FLinearColor colorVector = FLinearColor();
			MyDynamicMaterialInst->GetVectorParameterValue("SelectedPos", colorVector);
			FVector loc = colorVector;
			loc.Z = 20;
			for (int i = nameOfCube->arrayForMove.Num() - 1; i >= 0; i--) {
			if (nameOfCube->arrayForMove[i] != nullptr) {
			FVector locOfSquare = nameOfCube->arrayForMove[i]->GetActorLocation();
			vectorChangeForArrayOfMove(&locOfSquare);
			if (locOfSquare == loc) {
			includ = true;
			}
			}
			}
			if (includ == false) {
			for (int i = nameOfCube->arrayForMove.Num() - 1; i >= 0; i--) {
			if (nameOfCube->arrayForMove[i] != nullptr) nameOfCube->arrayForMove[i]->Destroy();
			nameOfCube->arrayForMove.RemoveAt(i);
			}
			while (nameOfCube->__road.size() != 0) {
			nameOfCube->__road.pop_front();
			}
			for (int i = nameOfCube->arrayForAttack.Num() - 1; i >= 0; i--) {
			if (nameOfCube->arrayForAttack[i] != nullptr) nameOfCube->arrayForAttack[i]->Destroy();
			nameOfCube->arrayForAttack.RemoveAt(i);
			}
			nameOfCube->MyDynamicMaterialInst->SetScalarParameterValue("ColorAlpha", 0);
			MyGameInterface->AttackButton->SetVisibility(ESlateVisibility::Collapsed);
			MyGameInterface->MoveButton->SetVisibility(ESlateVisibility::Collapsed);
			nameOfCube = nullptr;
			}
			}*/
		}
		else if (onFactory == true) {
			APlayerController * controller = GetWorld()->GetFirstPlayerController();
			FHitResult hit = FHitResult(ForceInit);
			bool bMouseResult = controller->GetHitResultUnderCursor(ECC_WorldStatic, true, hit);
			auto hitActor = hit.GetActor();
			if (hitActor != nullptr) {
				if (!hitActor->IsA(AMyFactory::StaticClass())) {
					onFactory = false;
					MyGameInterface->TankProductionButton->SetVisibility(ESlateVisibility::Collapsed);
				}
			}
		}
	}
	/*else if (value == -1) {
		APlayerController * controller = GetWorld()->GetFirstPlayerController();
		FHitResult hit = FHitResult(ForceInit);
		bool bMouseResult = controller->GetHitResultUnderCursor(ECC_WorldStatic, true, hit);
		auto hitActor = hit.GetActor();

		if (hitActor != nullptr) {
			if (hitActor->GetName() == "Floor") {
				FVector vector = hit.Location;
				vectorChangeForGrid(&vector);
				if (world[(int)vector.X][(int)vector.Y].empty == true) {
					world[(int)vector.X][(int)vector.Y].empty = false;
					vectorChangeFromGrid(&vector);
					FRotator Rotation(0, 0, 0);
					FActorSpawnParameters SpawnInfo;
					SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
					AMyPawn* cube = GetWorld()->SpawnActor<AMyPawn>(vector, Rotation, SpawnInfo);
					//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, vector.ToString());
					cube->myCamera = this;
					cube->loc = cube->GetActorLocation();
					cube->rot = cube->GetActorRotation();
					arrayOfCube.Add(cube);
				}
			}
		}
	}*/
}


static void vectorChangeForGrid(FVector* vector) {
	vector->X /= 100;
	vector->Y /= 100;
	vector->X = floor(vector->X);
	vector->Y = floor(vector->Y);
	vector->X += 0.5;
	vector->Y += 0.5;
}

static void vectorChangeForArrayOfMove(FVector* vector) {
	vector->X /= 100;
	vector->Y /= 100;
	vector->Z = 20;
}

static void vectorChangeFromGrid(FVector* vector) {
	vector->X *= 100;
	vector->Y *= 100;
	vector->Z = 20;
}

//MyGridWorld AMyCamera::world[20][20];



