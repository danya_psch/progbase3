// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PawnForAttack.h"
#include "SquareForMove.h"
#include "TankProjectile.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MyPawn.generated.h"

class SOMETHING_API AMyCamera;
enum class Team : uint8;

UENUM()
enum class EDirectionEnum : uint8
{
	VE_Down,
	VE_Up,
	VE_Right,
	VE_Left
};

UCLASS()
class SOMETHING_API AMyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	int health;

	int psevdoHeaith;

	int damage;

	Team team;

	ATankProjectile* tankProjectile;

	AMyCamera* myCamera;

	TArray<ASquareForMove*> arrayForMove;

	TArray<APawnForAttack*> arrayForAttack;

	float movement;

	bool inMove;

	bool inRotation;

	bool inRotationGun;

	EDirectionEnum direction;

	std::list<FVector> __road;

	FVector mainLocation;

	FVector loc;

	FVector newLoc;

	FRotator rot;

	FRotator newRot;

	FRotator rotForGun;

	FRotator newRotForGun;

	//FVector target;

	float forMove;
	//////////////////////
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* MyGun;

	UPROPERTY(VisibleAnywhere)
		UMaterial* MyMaterialForGun;

	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* MyDynamicMaterialInstForGun;
	//////////////////////

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* MyMesh;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* SpawnPlace;

	UPROPERTY(VisibleAnywhere)
		UMaterial* MyMaterial;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;

	//UPROPERTY(VisibleAnywhere)


	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* MyDynamicMaterialInst;

	UFUNCTION()
		void OnClicked(UPrimitiveComponent* Target, FKey ButtonPressed);

};
