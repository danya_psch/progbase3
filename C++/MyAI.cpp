// Fill out your copyright notice in the Description page of Project Settings.

#include "MyAI.h"
#include "MyCamera.h"
#include "MyGridWorld.h"
//#include "Engine.h"
#include "SquareForAi.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/UMG/Public/Components/SlateWrapperTypes.h"
#include "Runtime/Core/Public/Internationalization/Text.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/Core/Public/Containers/Queue.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"


// Sets default values
AMyAI::AMyAI()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CompStartLoc = FVector(0, 0, 0);
	Money = 200;
}

// Called when the game starts or when spawned
void AMyAI::BeginPlay()
{
	Super::BeginPlay();
	FString nameOfLevel = UGameplayStatics::GetCurrentLevelName(this);
	if (nameOfLevel == FString("MyWorld")) {
		CompStartLoc = FVector(1950, 2107, 20);
	}
	else if (nameOfLevel == FString("MyWorld_2")) {
		CompStartLoc = FVector(1850, 607 , 20);
	}
	else if (nameOfLevel == FString("MyWorld_3")) {
		CompStartLoc = FVector(950, 1107, 20);
	}
	
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	myFactory = GetWorld()->SpawnActor<AMyFactory>(CompStartLoc, FRotator(0, -90, 0), SpawnInfo);
	myFactory->team = Team::VE_Computer;
	myFactory->myCamera = nullptr;
}

// Called every frame
void AMyAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyAI::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AMyAI::running() {
	for (TPair<FString, AMyPawn*> tank : arrayOfTanks) {
		if (tank.Value != nullptr) {
			moveTank(tank.Value, 1);
		}
	}
	int x = (int)((CompStartLoc + FVector(0, -157, 0)).X / 100);
	int y = (int)((CompStartLoc + FVector(0, -157, 0)).Y / 100);
	AMyPawn* t = nullptr;
	while (Money >= 100) {
		if (MyMain->Player->world[x][y].empty == true) {
			MyMain->Player->world[x][y].empty = false;
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			AMyPawn* cube = GetWorld()->SpawnActor<AMyPawn>(CompStartLoc + FVector(0, -57, 0), FRotator(0, 90, 0), SpawnInfo);
			cube->myCamera = MyMain->Player;
			cube->mainLocation = CompStartLoc + FVector(0, -157, 0);
			cube->loc = cube->GetActorLocation();
			cube->rot = cube->GetActorRotation();
			cube->__road.push_front(CompStartLoc + FVector(0, -157, 0));
			cube->team = Team::VE_Computer;
			cube->newLoc = cube->__road.front();
			cube->inMove = true;

			cube->MyDynamicMaterialInst->SetScalarParameterValue("ColorAlpha", -1);
			moveTank(cube, -1);

			//t = cube;
			arrayOfTanks.Add(cube->GetName(), cube);
			MyMain->allTanks.Add(cube->GetName(), cube);
			//FGenericPlatformProcess::ConditionalSleep(true, 100);
			Money -= 100;
		}
		//Money -= 100;
		if (MyMain->Player->world[x][y].empty == false) break;
	}
}

void AMyAI::moveTank(AMyPawn* tank, int qq) {


	if (qq != -1) {
		for (TPair<FString, AMyPawn*> _tank : MyMain->Player->arrayOfTanks) {

			if (tank->GetDistanceTo(_tank.Value) <= 400 && _tank.Value->inMove == false && _tank.Value->inRotation == false) {
				_tank.Value->psevdoHeaith -= tank->damage;
				if (_tank.Value->psevdoHeaith <= 0) {
					//tank->myCamera->arrayOfTanks.FindAndRemoveChecked(_tank.Value->GetName());
					tank->myCamera->arrayOfTanks.Remove(_tank.Value->GetName());
				}
				//MyMain->Player->arrayOfTanks.FindAndRemoveChecked(_tank.Value->GetName());


				FActorSpawnParameters SpawnInfo;
				SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				FVector loc = _tank.Value->GetActorLocation();
				loc.Z = 100;



				APawnForAttack* pawnForAttack = GetWorld()->SpawnActor<APawnForAttack>(loc, FRotator(-180, 0, 0), SpawnInfo);
				pawnForAttack->father = tank;
				pawnForAttack->attachedTank = _tank.Value;
				tank->arrayForAttack.Add(pawnForAttack);
				/////////////////////
				tank->movement -= 3;
				FRotator rotator = UKismetMathLibrary::FindLookAtRotation(tank->GetActorLocation(), _tank.Value->GetActorLocation()) - FRotator(0, 180, 0);
				tank->arrayForAttack[0]->newRot = rotator;
				tank->arrayForAttack[0]->rot = tank->GetActorRotation();
				tank->arrayForAttack[0]->inRotationForAttack = true;

				for (int i = tank->arrayForAttack.Num() - 1; i >= 0; i--) {
					if (tank->arrayForAttack[i] != nullptr) tank->arrayForAttack[i];
				}
				//tank->myCamera->MyGameInterface->EndTurnButton->SetVisibility(ESlateVisibility::HitTestInvisible);
				MyMain->inMove++;
				break;

				/////////////////////
			}
		}
	}

	FVector loc = tank->mainLocation;
	loc.Z = 20;

	for (int i = 0; i < MyMain->size_x; i++) {
		for (int j = 0; j < MyMain->size_y; j++) {
			if (MyMain->Player->world[i][j].empty == false) {
				MyMain->Player->world[i][j].passability = false;
			}
		}
	}



	TArray<SquareForAi*> arrayForMove;
	TQueue<SquareForAi*> queue;
	queue.Enqueue(new SquareForAi(0, loc, nullptr));

	while (!queue.IsEmpty()) {
		SquareForAi* curr;
		queue.Peek(curr);
		queue.Pop();

		int x = curr->loc.X / 100;
		int y = curr->loc.Y / 100;

		if (x < MyMain->size_x - 1 && curr->level < tank->movement) {
			if (MyMain->Player != nullptr && MyMain->Player->world[x + 1][y].passability == true) {
				MyMain->Player->world[x + 1][y].passability = false;
				FVector location(x * 100 + 150, y * 100 + 50, 20);
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, location.ToString());
				SquareForAi* a = new SquareForAi(curr->level + 1, location, curr);
				queue.Enqueue(a);
				arrayForMove.Add(a);
			}
		}
		if (y < MyMain->size_y - 1 && curr->level < tank->movement) {
			if (MyMain->Player != nullptr && MyMain->Player->world[x][y + 1].passability == true) {
				MyMain->Player->world[x][y + 1].passability = false;
				FVector location(x * 100 + 50, y * 100 + 150, 20);
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, location.ToString());
				SquareForAi* a = new SquareForAi(curr->level + 1, location, curr);
				queue.Enqueue(a);
				arrayForMove.Add(a);
			}
		}
		if (y > 0 && curr->level < tank->movement) {
			if (MyMain->Player != nullptr && MyMain->Player->world[x][y - 1].passability == true) {
				MyMain->Player->world[x][y - 1].passability = false;
				FVector location(x * 100 + 50, y * 100 - 50, 20);
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, location.ToString());
				SquareForAi* a = new SquareForAi(curr->level + 1, location, curr);
				queue.Enqueue(a);
				arrayForMove.Add(a);
			}
		}
		if (x > 0 && curr->level < tank->movement) {
			if (MyMain->Player != nullptr && MyMain->Player->world[x - 1][y].passability == true) {
				MyMain->Player->world[x - 1][y].passability = false;
				FVector location(x * 100 - 50, y * 100 + 50, 20);
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, location.ToString());
				SquareForAi* a = new SquareForAi(curr->level + 1, location, curr);
				queue.Enqueue(a);
				arrayForMove.Add(a);
			}
		}
	}

	for (int i = 0; i < MyMain->size_x; i++) {
		for (int j = 0; j < MyMain->size_y; j++) {
			MyMain->Player->world[i][j].passability = true;
		}
	}

	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("!"));
	if (arrayForMove.Num() != 0) {
		MyMain->inMove++;
		SquareForAi* target = arrayForMove[0];
		if (MyMain != nullptr) {
			float minLength = 4000;
			for (int j = 0; j < MyMain->allResourceTower.Num(); j++) {
				if (MyMain->allResourceTower[j]->team != Team::VE_Computer) {
					for (int i = 0; i < arrayForMove.Num(); i++) {
						float length = FVector::Distance(arrayForMove[i]->loc, MyMain->allResourceTower[j]->GetActorLocation());
						if (length <= minLength) {
							minLength = length;
							target = arrayForMove[i];
						}
					}
				}
			}

			/*FVector tankLoc = tank->GetActorLocation();
			MyMain->Player->world[(int)(tankLoc.X / 100)][(int)(tankLoc.Y / 100)].empty = true;
			tank->SetActorLocation(target->loc);
			tank->mainLocation = target->loc;
			MyMain->Player->world[(int)(target->loc.X / 100)][(int)(target->loc.Y / 100)].empty = false;*/
	
			FVector tankLoc = tank->mainLocation;
			MyMain->Player->world[(int)(tankLoc.X / 100)][(int)(tankLoc.Y / 100)].empty = true;

			if (qq == 1) {
				SquareForAi* cur = target;
				while (cur->father != nullptr) {
					tank->__road.push_front(cur->loc);
					cur = cur->father;
				}
			}
			else if (qq == -1) {
				SquareForAi* cur = target;
				std::list<FVector> roadd;
				//roadd.push_front(cur->loc);
				while (cur->father != nullptr) {
					roadd.push_front(cur->loc);
					cur = cur->father;
				}
				while (roadd.size() != 0) {
					tank->__road.push_back(roadd.front());
					roadd.pop_front();
				}
			}
			tank->mainLocation = tank->__road.back();
			int _x_ = tank->mainLocation.X / 100;
			int _y_ = tank->mainLocation.Y / 100;
			MyMain->Player->world[_x_][_y_].empty = false;

			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::FromInt(tank->mainLocation.X / 100) + FString::FromInt(tank->mainLocation.Y / 100));
			/*SquareForAi* cur = target;
			while (cur->father != nullptr) {
				tank->__road.push_front(cur->loc);
				cur = cur->father;
			}*/

			for (int i = 0; i < arrayForMove.Num(); i++) {
				delete arrayForMove[i];
			}

			float x = tank->GetActorLocation().X - (tank->__road.front().X);
			if (x > 0) {
				tank->newRot = FRotator(0, 0, 0);
			}
			else if (x < 0) {
				tank->newRot = FRotator(0, 180, 0);
			}
			else {
				float y = tank->GetActorLocation().Y - (tank->__road.front().Y);
				if (y < 0) {
					tank->newRot = FRotator(0, -90, 0);
				}
				else if (y > 0) {
					tank->newRot = FRotator(0, 90, 0);
				}
			}
			tank->rot = tank->GetActorRotation();
			tank->inRotation = true;
			tank->newLoc = tank->__road.front();

			tank->movement -= target->level;
		}
		

	}

	///////////
	/*std::list<FVector> list;
	SquareForAi* cur = target;
	while (target->father != nullptr) {
		list.push_front(cur->loc);
		cur = cur->father;
	}

	while (list.size() != 0) {
		tank->__road.push_back(list.back());
		list.pop_back();
	}

	tank->mainLocation = tank->__road.back();
	MyMain->Player->world[(int)(tank->mainLocation.X / 100)][(int)(tank->mainLocation.X / 100)].empty = false;*/
	/////////////////////////////////////
}
