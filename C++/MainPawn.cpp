// Fill out your copyright notice in the Description page of Project Settings.

#include "MainPawn.h"
#include "MyCamera.h"
#include "MyPawn.h"
#include "MyResourceTower.h"
#include "MyAI.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"


// Sets default values
AMainPawn::AMainPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMainPawn::BeginPlay()
{

	Super::BeginPlay();

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	FString nameOfLevel = UGameplayStatics::GetCurrentLevelName(this);


	if (nameOfLevel == FString("MyWorld")) {
		size_x = 20;
		size_y = 20;
		AMyResourceTower* resourceTower;
		resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(750, 750, 20), FRotator(0, 0, 0), SpawnInfo);
		resourceTower->team = Team::VE_None;
		allResourceTower.Add(resourceTower);
		resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(1250, 1250, 20), FRotator(0, 0, 0), SpawnInfo);
		resourceTower->team = Team::VE_None;
		allResourceTower.Add(resourceTower);
		resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(450, 1550, 20), FRotator(0, 0, 0), SpawnInfo);
		resourceTower->team = Team::VE_None;
		allResourceTower.Add(resourceTower);
		resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(1550, 450, 20), FRotator(0, 0, 0), SpawnInfo);
		resourceTower->team = Team::VE_None;
		allResourceTower.Add(resourceTower);
	}
	else if (nameOfLevel == FString("MyWorld_2")) {
		size_x = 19;
		size_y = 5;
		AMyResourceTower* resourceTower;
		resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(1450, 250, 20), FRotator(0, 0, 0), SpawnInfo);
		resourceTower->team = Team::VE_None;
		allResourceTower.Add(resourceTower);
		resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(950, 250, 20), FRotator(0, 0, 0), SpawnInfo);
		resourceTower->team = Team::VE_None;
		allResourceTower.Add(resourceTower);
		resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(450, 250, 20), FRotator(0, 0, 0), SpawnInfo);
		resourceTower->team = Team::VE_None;
		allResourceTower.Add(resourceTower);
		
	}
	else if (nameOfLevel == FString("MyWorld_3")) {
		size_x = 10;
		size_y = 10;
		AMyResourceTower* resourceTower;
		resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(450, 250, 20), FRotator(0, 0, 0), SpawnInfo);
		resourceTower->team = Team::VE_None;
		allResourceTower.Add(resourceTower);
		resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(550, 750, 20), FRotator(0, 0, 0), SpawnInfo);
		resourceTower->team = Team::VE_None;
		allResourceTower.Add(resourceTower);
	}

	/*AMyResourceTower* resourceTower;
	resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(750, 750, 20), FRotator(0, 0, 0), SpawnInfo);
	resourceTower->team = Team::VE_None;
	allResourceTower.Add(resourceTower);
	resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(1250, 1250, 20), FRotator(0, 0, 0), SpawnInfo);
	resourceTower->team = Team::VE_None;
	allResourceTower.Add(resourceTower);
	resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(450, 1550, 20), FRotator(0, 0, 0), SpawnInfo);
	resourceTower->team = Team::VE_None;
	allResourceTower.Add(resourceTower);
	resourceTower = GetWorld()->SpawnActor<AMyResourceTower>(FVector(1550, 450, 20), FRotator(0, 0, 0), SpawnInfo);
	resourceTower->team = Team::VE_None;
	allResourceTower.Add(resourceTower);*/

	Player = GetWorld()->SpawnActor<AMyCamera>(FVector(0, 0, 100), FRotator(0, 0, 0), SpawnInfo);
	Player->MyMain = this;

	Ai = GetWorld()->SpawnActor<AMyAI>(FVector(2000, 2000, 100), FRotator(0, 0, 0), SpawnInfo);
	Ai->MyMain = this;
}

// Called every frame
void AMainPawn::Tick(float DeltaTime)
{
	//bool status = false;
	Super::Tick(DeltaTime);
	if (inMove == 0) {
		Player->MyGameInterface->EndTurnButton->SetVisibility(ESlateVisibility::Visible);
	}
	else {
		Player->MyGameInterface->EndTurnButton->SetVisibility(ESlateVisibility::HitTestInvisible);
	}
}


// Called to bind functionality to input
void AMainPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}


