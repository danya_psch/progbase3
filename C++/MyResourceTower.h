// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MainPawn.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MyResourceTower.generated.h"

UCLASS()
class SOMETHING_API AMyResourceTower : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyResourceTower();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	Team team;

	int quantityOfPlayerTanks;

	int quantityOfComputerTanks;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* MyMesh;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* verge_1;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* verge_2;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* verge_3;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* verge_4;

	UPROPERTY(VisibleAnywhere)
		UMaterial* MyMaterial;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;

	UPROPERTY(VisibleAnywhere)
		UMaterial* MyMaterialForVerge;

	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* MyDynamicMaterialInstForVerge_1;

	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* MyDynamicMaterialInstForVerge_2;

	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* MyDynamicMaterialInstForVerge_3;

	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* MyDynamicMaterialInstForVerge_4;

	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* MyDynamicMaterialInst;

};

