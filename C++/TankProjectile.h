// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Components/TimelineComponent.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "Runtime/Engine/Classes/Curves/CurveVector.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "TankProjectile.generated.h"

class SOMETHING_API AMyPawn;

UCLASS()
class SOMETHING_API ATankProjectile : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATankProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY()
		UTimelineComponent* MyTimeline;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* MyMesh;

	UPROPERTY(VisibleAnywhere)
		UMaterial* MyMaterial;

	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* MyDynamicMaterialInst;

	AMyPawn* father;

	AMyPawn* target;

	bool inMove;


	void Initialize(FVector _startLocation, FVector _endLocation);
	//////////////

	UPROPERTY(EditAnywhere)
		UCurveFloat* fCurve;

	UPROPERTY(EditAnywhere)
		UCurveVector* vCurve;

	FVector startLocation;

	FVector endLocation;

	FOnTimelineVector InterpFunction{};

	FOnTimelineEvent TimeLineFinished{};

	UFUNCTION()
		void TimeLineVectorReturn(FVector vector);

	UFUNCTION()
		void OnTimeLineFinished();

};

