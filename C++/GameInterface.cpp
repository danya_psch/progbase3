// Fill out your copyright notice in the Description page of Project Settings.


#include "GameInterface.h"
#include "MyCamera.h"
#include "MainPawn.h"
#include "MyAI.h"
#include "MyPawn.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

void UGameInterface::NativeConstruct()
{
	Super::NativeConstruct();

	MoveButton->OnClicked.AddDynamic(this, &UGameInterface::OnMoveButton);
	AttackButton->OnClicked.AddDynamic(this, &UGameInterface::OnAttackButton);
	EndTurnButton->OnClicked.AddDynamic(this, &UGameInterface::OnEndTurnButton);
	TankProductionButton->OnClicked.AddDynamic(this, &UGameInterface::OnTankProductionButton);
	Button_ToMainMenu->OnClicked.AddDynamic(this, &UGameInterface::ToMainMenu);
	
}

void UGameInterface::OnMoveButton() {
	//
	if (myCamera->nameOfCube != nullptr) {
		if (myCamera->nameOfCube->arrayForMove.Num() != 0) {
			for (int i = myCamera->nameOfCube->arrayForMove.Num() - 1; i >= 0; i--) {
				if (myCamera->nameOfCube->arrayForMove[i] != nullptr) myCamera->nameOfCube->arrayForMove[i]->Destroy();
				myCamera->nameOfCube->arrayForMove.RemoveAt(i);
			}
		}

		if (myCamera->nameOfCube->arrayForAttack.Num() != 0) {
			for (int i = myCamera->nameOfCube->arrayForAttack.Num() - 1; i >= 0; i--) {
				if (myCamera->nameOfCube->arrayForAttack[i] != nullptr)  myCamera->nameOfCube->arrayForAttack[i]->Destroy();
				myCamera->nameOfCube->arrayForAttack.RemoveAt(i);
			}
		}

		while (myCamera->nameOfCube->__road.size() != 0) {
			myCamera->nameOfCube->__road.pop_front();
		}
	}
	if (myCamera->nameOfCube != nullptr) {
		for (int i = 0; i < myCamera->MyMain->size_x; i++) {
			for (int j = 0; j < myCamera->MyMain->size_y; j++) {
				if (myCamera->world[i][j].empty == false) {
					myCamera->world[i][j].passability = false;
				}
			}
		}

		FVector loc = myCamera->nameOfCube->GetActorLocation();
		loc.Z = 20;

		TQueue<ASquareForMove*> queue;

		int x = loc.X / 100;
		int y = loc.Y / 100;

		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		FRotator Rotation(0, 0, 0);

		ASquareForMove* c = GetWorld()->SpawnActor<ASquareForMove>(loc, Rotation, SpawnInfo);
		c->Initialize(nullptr, myCamera->world[(int)x][(int)y], 0, (int)x, (int)y, myCamera->nameOfCube);
		queue.Enqueue(c);
		myCamera->nameOfCube->arrayForMove.Add(c);

		while (!queue.IsEmpty()) {
			ASquareForMove* cur;
			queue.Peek(cur);
			queue.Pop();
			if (cur != nullptr) {
				if (cur->x < myCamera->MyMain->size_x - 1 && cur->lenghtOfWay < myCamera->nameOfCube->movement) {
					if (myCamera != nullptr && myCamera->world[cur->x + 1][cur->y].passability == true) {
						myCamera->world[cur->x + 1][cur->y].passability = false;
						FVector l(cur->x * 100 + 150, cur->y * 100 + 50, 20);
						ASquareForMove* a = GetWorld()->SpawnActor<ASquareForMove>(l, Rotation, SpawnInfo);
						a->Initialize(cur, myCamera->world[cur->x + 1][cur->y], cur->lenghtOfWay + 1, cur->x + 1, cur->y, myCamera->nameOfCube);
						queue.Enqueue(a);
						myCamera->nameOfCube->arrayForMove.Add(a);
					}
				}
				if (cur->y < myCamera->MyMain->size_y - 1 && cur->lenghtOfWay < myCamera->nameOfCube->movement) {
					if (myCamera != nullptr && myCamera->world[cur->x][cur->y + 1].passability == true) {
						myCamera->world[cur->x][cur->y + 1].passability = false;
						FVector l(cur->x * 100 + 50, cur->y * 100 + 150, 20);
						ASquareForMove* a = GetWorld()->SpawnActor<ASquareForMove>(l, Rotation, SpawnInfo);
						a->Initialize(cur, myCamera->world[cur->x][cur->y + 1], cur->lenghtOfWay + 1, cur->x, cur->y + 1, myCamera->nameOfCube);
						queue.Enqueue(a);
						myCamera->nameOfCube->arrayForMove.Add(a);
					}
				}
				if (cur->y > 0 && cur->lenghtOfWay < myCamera->nameOfCube->movement) {
					if (myCamera != nullptr && myCamera->world[cur->x][cur->y - 1].passability == true) {
						myCamera->world[cur->x][cur->y - 1].passability = false;
						FVector l(cur->x * 100 + 50, cur->y * 100 - 50, 20);
						ASquareForMove* a = GetWorld()->SpawnActor<ASquareForMove>(l, Rotation, SpawnInfo);
						a->Initialize(cur, myCamera->world[cur->x][cur->y - 1], cur->lenghtOfWay + 1, cur->x, cur->y - 1, myCamera->nameOfCube);
						queue.Enqueue(a);
						myCamera->nameOfCube->arrayForMove.Add(a);
					}
				}
				if (cur->x > 0 && cur->lenghtOfWay < myCamera->nameOfCube->movement) {
					if (myCamera != nullptr && myCamera->world[cur->x - 1][cur->y].passability == true) {
						myCamera->world[cur->x - 1][cur->y].passability = false;
						FVector l(cur->x * 100 - 50, cur->y * 100 + 50, 20);
						ASquareForMove* a = GetWorld()->SpawnActor<ASquareForMove>(l, Rotation, SpawnInfo);
						a->Initialize(cur, myCamera->world[cur->x - 1][cur->y], cur->lenghtOfWay + 1, cur->x - 1, cur->y, myCamera->nameOfCube);
						queue.Enqueue(a);
						myCamera->nameOfCube->arrayForMove.Add(a);
					}
				}
			}
		}

		for (int i = 0; i < myCamera->MyMain->size_x; i++) {
			for (int j = 0; j < myCamera->MyMain->size_y; j++) {
				if (myCamera != nullptr) myCamera->world[i][j].passability = true;
			}
		}
	}
	//
	
}

void UGameInterface::OnAttackButton() {
	//
	if (myCamera->nameOfCube != nullptr) {
		if (myCamera->nameOfCube->arrayForMove.Num() != 0) {
			for (int i = myCamera->nameOfCube->arrayForMove.Num() - 1; i >= 0; i--) {
				if (myCamera->nameOfCube->arrayForMove[i] != nullptr) myCamera->nameOfCube->arrayForMove[i]->Destroy();
				myCamera->nameOfCube->arrayForMove.RemoveAt(i);
			}
		}

		if (myCamera->nameOfCube->arrayForAttack.Num() != 0) {
			for (int i = myCamera->nameOfCube->arrayForAttack.Num() - 1; i >= 0; i--) {
				if (myCamera->nameOfCube->arrayForAttack[i] != nullptr)  myCamera->nameOfCube->arrayForAttack[i]->Destroy();
				myCamera->nameOfCube->arrayForAttack.RemoveAt(i);
			}
		}

		while (myCamera->nameOfCube->__road.size() != 0) {
			myCamera->nameOfCube->__road.pop_front();
		}
	}
	if (myCamera->nameOfCube != nullptr) {
		if (myCamera->nameOfCube->movement > 0) {
			
			for (TPair<FString, AMyPawn*> tank : myCamera->MyMain->Ai->arrayOfTanks) {
				
				if (myCamera->nameOfCube->GetDistanceTo(tank.Value) <= 400 && tank.Value->inMove == false && tank.Value->inRotation == false) {
					FActorSpawnParameters SpawnInfo;
					SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
					FVector loc = tank.Value->GetActorLocation();
					loc.Z = 100;

					APawnForAttack* pawnForAttack = GetWorld()->SpawnActor<APawnForAttack>(loc, FRotator(-180, 0, 0), SpawnInfo);
					pawnForAttack->father = myCamera->nameOfCube;
					pawnForAttack->attachedTank = tank.Value;
					myCamera->nameOfCube->arrayForAttack.Add(pawnForAttack);
				}
				
			}
		}
	}
	//
}

void UGameInterface::OnTankProductionButton() {
	//
	if (myCamera->Money >= 100) {
		if (myCamera->onFactory == true) {
			if (myCamera->world[0][0].empty == true) {
				myCamera->world[0][0].empty = false;
				FActorSpawnParameters SpawnInfo;
				SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				AMyPawn* cube = GetWorld()->SpawnActor<AMyPawn>(myCamera->PlayerStartLoc + FVector(0, 57, 0), FRotator(0, -90, 0), SpawnInfo);
				cube->myCamera = myCamera;
				cube->loc = cube->GetActorLocation();
				cube->rot = cube->GetActorRotation();
				cube->__road.push_front(myCamera->PlayerStartLoc + FVector(0, 157, 0));
				cube->newLoc = cube->__road.front();
				cube->inMove = true;
				myCamera->MyMain->inMove++;
				cube->team = Team::VE_Player;
				cube->mainLocation = cube->__road.back();
				myCamera->arrayOfCube.Add(cube);
				myCamera->arrayOfTanks.Add(cube->GetName(),cube);
				myCamera->MyMain->allTanks.Add(cube->GetName(), cube);
				myCamera->Money -= 100;
				MoneyTextBlock->SetText(FText::FromString(FString::FromInt(myCamera->Money)));
			}
		}
	}
	//
}



void UGameInterface::OnEndTurnButton() {
	//


	myCamera->MyMain->Ai->running();


	for (TPair<FString, AMyPawn*> tank : myCamera->MyMain->allTanks) {
		tank.Value->movement = 3;
	}
	myCamera->Money += 100;
	myCamera->MyMain->Ai->Money += 100;
	for (AMyResourceTower* resourceTower : myCamera->MyMain->allResourceTower) {
		for (TPair<FString, AMyPawn*> tank : myCamera->MyMain->allTanks) {
			if (FVector::Dist(resourceTower->GetActorLocation(), tank.Value->mainLocation) <= 150) {

				switch (tank.Value->team)
				{
				case Team::VE_Player: {
					resourceTower->quantityOfPlayerTanks++;
					break;
				}
				case Team::VE_Computer: {
					resourceTower->quantityOfComputerTanks++;
					break;
				}
				default:
					break;
				}
			}
		}
		if (resourceTower->quantityOfComputerTanks == 0 && resourceTower->quantityOfPlayerTanks == 0) {
			float a;
			resourceTower->MyDynamicMaterialInstForVerge_1->GetScalarParameterValue("Alpha", a);
			if (a != 0) {
				if (a > 0) {
					myCamera->Money += 100;
				}
				else {
					myCamera->MyMain->Ai->Money += 100;
				}
			}
		}
		else if (resourceTower->quantityOfComputerTanks == 0) {
			resourceTower->MyDynamicMaterialInstForVerge_1->SetScalarParameterValue("Alpha", 1);
			resourceTower->MyDynamicMaterialInstForVerge_2->SetScalarParameterValue("Alpha", 1);
			resourceTower->MyDynamicMaterialInstForVerge_3->SetScalarParameterValue("Alpha", 1);
			resourceTower->MyDynamicMaterialInstForVerge_4->SetScalarParameterValue("Alpha", 1);
			myCamera->Money += 100;
			resourceTower->team = Team::VE_Player;
		}
		else if (resourceTower->quantityOfPlayerTanks == 0) {
			resourceTower->MyDynamicMaterialInstForVerge_1->SetScalarParameterValue("Alpha", -1);
			resourceTower->MyDynamicMaterialInstForVerge_2->SetScalarParameterValue("Alpha", -1);
			resourceTower->MyDynamicMaterialInstForVerge_3->SetScalarParameterValue("Alpha", -1);
			resourceTower->MyDynamicMaterialInstForVerge_4->SetScalarParameterValue("Alpha", -1);
			myCamera->MyMain->Ai->Money += 100;
			resourceTower->team = Team::VE_Computer;
		}
		resourceTower->quantityOfComputerTanks = 0;
		resourceTower->quantityOfPlayerTanks = 0;
	}

	int quantityForPlayer = 0;
	int quantityForComputer = 0;
	for (int i = 0; i < myCamera->MyMain->allResourceTower.Num(); i++) {
		if (myCamera->MyMain->allResourceTower[i]->team == Team::VE_Player) {
			quantityForPlayer++;
		}
		else if (myCamera->MyMain->allResourceTower[i]->team == Team::VE_Computer) {
			quantityForComputer++;
		}
	}
	if (quantityForPlayer == myCamera->MyMain->allResourceTower.Num() || quantityForComputer == myCamera->MyMain->allResourceTower.Num()) {
		Image_YouDied->SetVisibility(ESlateVisibility::Visible);
		Button_ToMainMenu->SetVisibility(ESlateVisibility::Visible);

		APlayerController * controller = GetWorld()->GetFirstPlayerController();

		if (controller != nullptr)
		{
			controller->SetPause(true);
		}
	}

	/*if (myCamera->MyMain->allResourceTower[0]->team == Team::VE_Player &&
		myCamera->MyMain->allResourceTower[1]->team == Team::VE_Player &&
		myCamera->MyMain->allResourceTower[2]->team == Team::VE_Player &&
		myCamera->MyMain->allResourceTower[3]->team == Team::VE_Player
		) {
		Image_YouDied->SetVisibility(ESlateVisibility::Visible);
		Button_ToMainMenu->SetVisibility(ESlateVisibility::Visible);

		APlayerController * controller = GetWorld()->GetFirstPlayerController();
		
		if (controller != nullptr)
		{
			controller->SetPause(true);
		}

		//UGameplayStatics::OpenLevel(GetWorld(), "MainMenu");
	}
	else if (myCamera->MyMain->allResourceTower[0]->team == Team::VE_Computer &&
		myCamera->MyMain->allResourceTower[1]->team == Team::VE_Computer &&
		myCamera->MyMain->allResourceTower[2]->team == Team::VE_Computer &&
		myCamera->MyMain->allResourceTower[3]->team == Team::VE_Computer
		) {
		Image_YouDied->SetVisibility(ESlateVisibility::Visible);
		Button_ToMainMenu->SetVisibility(ESlateVisibility::Visible);

		APlayerController * controller = GetWorld()->GetFirstPlayerController();

		if (controller != nullptr)
		{
			controller->SetPause(true);
		}

	}*/


	MoneyTextBlock->SetText(FText::FromString(FString::FromInt(myCamera->Money)));

	//myCamera->MyMain->Ai->running();
	//
}


void UGameInterface::ToMainMenu() {
	UGameplayStatics::OpenLevel(GetWorld(), "MainMenu");
}

