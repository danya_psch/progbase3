// Fill out your copyright notice in the Description page of Project Settings.

#include "TankProjectile.h"
#include "MyPawn.h"
#include "MyCamera.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/TimelineComponent.h"


// Sets default values
ATankProjectile::ATankProjectile()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	inMove = true;
	MyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = MyMesh;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> FoundMesh(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));//"StaticMesh'/Engine/BasicShapes/Cube.Cube'"
	if (FoundMesh.Succeeded())
	{
		MyMesh->SetStaticMesh(FoundMesh.Object);
	}

	FVector vect = FVector(0.1, 0.1, 0.1);
	MyMesh->SetRelativeScale3D(vect);

	//////////////////////////////////

	static ConstructorHelpers::FObjectFinder<UMaterial> FoundMaterial(TEXT("/Game/MyContent/ForTankProjectile.ForTankProjectile")); //"Material'/Engine/EngineMaterials/CubeMaterial.CubeMaterial'"
	if (FoundMaterial.Succeeded())//Engine/VREditor/BasicMeshes/M_Floor_01.M_Floor_01
	{
		MyMaterial = FoundMaterial.Object;
	}
	MyDynamicMaterialInst = UMaterialInstanceDynamic::Create(MyMaterial, MyMesh);

	MyMesh->SetMaterial(0, MyDynamicMaterialInst);

	MyTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("TimeLine"));

	InterpFunction.BindUFunction(this, FName("TimeLineVectorReturn"));
	TimeLineFinished.BindUFunction(this, FName("OnTimeLineFinished"));

	/*static ConstructorHelpers::FObjectFinder<UCurveFloat> Curvy(TEXT("CurveFloat'/Game/MyContent/Curves/fCurve.fCurve'"));
	if (Curvy.Object != NULL) {
	fCurve = Curvy.Object;
	}*/

	static ConstructorHelpers::FObjectFinder<UCurveVector> Curvy(TEXT("CurveVector'/Game/MyContent/Curves/vCurve.vCurve'"));
	if (Curvy.Object != nullptr) {
		vCurve = Curvy.Object;
	}
}

void ATankProjectile::Initialize(FVector _startLocation, FVector _endLocation) {
	startLocation = _startLocation;
	endLocation = _endLocation;
}

// Called when the game starts or when spawned
void ATankProjectile::BeginPlay()
{
	Super::BeginPlay();
	if (vCurve) {
		//MyTimeline->AddInterpFloat(fCurve, InterpFunction, FName("Alpha"));
		MyTimeline->AddInterpVector(vCurve, InterpFunction, FName("Alpha"));
		MyTimeline->SetTimelineFinishedFunc(TimeLineFinished);

		MyTimeline->SetLooping(false);
		MyTimeline->SetIgnoreTimeDilation(true);

		MyTimeline->Play();
	}
}

// Called every frame
void ATankProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATankProjectile::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

/*void ATankProjectile::TimeLineFloatReturn(float value)
{
SetActorLocation(FMath::Lerp(startLocation, endLocation, value));
}*/

void ATankProjectile::TimeLineVectorReturn(FVector vector)
{
	SetActorLocation(FVector(FMath::Lerp(startLocation.X, endLocation.X, vector.X),
		FMath::Lerp(startLocation.Y, endLocation.Y, vector.Y),
		FMath::Lerp(startLocation.Z, endLocation.Z, vector.Z))
	);
}

void ATankProjectile::OnTimeLineFinished()
{
	inMove = false;
}

