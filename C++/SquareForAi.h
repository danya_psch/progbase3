// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Core/Public/Math/Vector.h"

/**
 * 
 */
class SOMETHING_API SquareForAi
{
public:
	SquareForAi(int _level, FVector _loc, SquareForAi* _father);
	SquareForAi();
	~SquareForAi();

	int level;

	FVector loc;

	SquareForAi* father;
};
