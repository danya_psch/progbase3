// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/Core/Public/Containers/Array.h"
#include "Runtime/Core/Public/Containers/Map.h"
#include "Runtime/Core/Public/Math/Vector.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MainPawn.generated.h"


class SOMETHING_API AMyResourceTower;
class SOMETHING_API AMyCamera;
class SOMETHING_API AMyPawn;
class SOMETHING_API AMyAI;
//class SOMETHING_API AMyResourceTower;

UENUM()
enum class Team : uint8
{
	VE_Player,
	VE_Computer,
	VE_None
};


UCLASS()
class SOMETHING_API AMainPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMainPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	int size_x;
	int size_y;

	TMap<FString, AMyPawn*> allTanks;

	AMyCamera* Player;

	AMyAI* Ai;

	TArray<AMyResourceTower*> allResourceTower;

	int inMove;
};

