// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/Engine/Classes/Materials/Material.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Engine/StaticMeshActor.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectBaseUtility.h"
#include "MyGridWorld.h"
#include "MyFactory.h"
#include "Runtime/Core/Public/Containers/Map.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "GameInterface.h"
#include "MainPawn.h"
#include "MyAI.h"
#include "MyResourceTower.h"
#include "MyPawn.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MyCamera.generated.h"

UCLASS()
class SOMETHING_API AMyCamera : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyCamera();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere)
		class UCurveFloat* fCurve;

	UPROPERTY(VisibleAnywhere)
		TArray<AMyPawn*> arrayOfCube;

	TMap<FString, AMyPawn*> arrayOfTanks;

	AMyFactory* myFactory;

	bool onFactory;

	AMainPawn* MyMain;

	int Money;

	bool forMenu;

	FVector PlayerStartLoc;

	FString nameOfLevel;

	AMyPawn* nameOfCube;

	MyGridWorld world[20][20];

	UPROPERTY(EditAnywhere)
		UCameraComponent* OurCamera;

	UPROPERTY(VisibleAnywhere)
		UMaterial* MyMaterial;

	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* MyDynamicMaterialInst;
	/////////////////////////////
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UGameInterface> wGameInterface;

	UGameInterface* MyGameInterface;
	/////////////////////////////
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> wPauseMenu;

	UUserWidget* MyPauseMenu;
	/////////////////////////////

	UPROPERTY(EditAnywhere)
		float MaxSpeed;

	UFUNCTION()
		void MoveForward(float Val);

	UFUNCTION()
		void MoveRight(float Val);

	UFUNCTION()
		void MouseButtons(float Val);

	UFUNCTION()
		void Pause(float Val);


};
