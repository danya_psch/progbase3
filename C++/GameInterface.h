// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/UMG/Public/Components/Button.h"
#include "Runtime/UMG/Public/Components/HorizontalBox.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Runtime/UMG/Public/Components/Image.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameInterface.generated.h"

/**
 * 
 */



UCLASS()
class SOMETHING_API UGameInterface : public UUserWidget
{
	GENERATED_BODY()
	
public:
	virtual void NativeConstruct() override;

	UPROPERTY(Meta = (BindWidget))
	UImage* Image_YouDied;

	UPROPERTY(Meta = (BindWidget))
	UButton* Button_ToMainMenu;

	UPROPERTY(Meta = (BindWidget))
	UTextBlock* MoneyTextBlock;

	UPROPERTY(Meta = (BindWidget))
	UTextBlock* HealthTextBlock;

	UPROPERTY(Meta = (BindWidget))
	UTextBlock* MovementTextBlock;

	UPROPERTY(Meta = (BindWidget))
	UHorizontalBox* HorizontalBoxForTank;

	UPROPERTY(Meta = (BindWidget))
	UButton* MoveButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* AttackButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* EndTurnButton;

	UPROPERTY(Meta = (BindWidget))
	UButton* TankProductionButton;

	class SOMETHING_API AMyCamera* myCamera;
private:
	UFUNCTION()
	void OnMoveButton();

	UFUNCTION()
	void OnAttackButton();
	
	UFUNCTION()
	void OnEndTurnButton();

	UFUNCTION()
	void OnTankProductionButton();

	UFUNCTION()
	void ToMainMenu();
};
