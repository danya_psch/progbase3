// Fill out your copyright notice in the Description page of Project Settings.

#include "MyResourceTower.h"
#include "UObject/ConstructorHelpers.h"


// Sets default values
AMyResourceTower::AMyResourceTower()
{

	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	quantityOfPlayerTanks = 0;
	quantityOfComputerTanks = 0;
	MyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	verge_1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("verge_1"));
	verge_2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("verge_2"));
	verge_3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("verge_3"));
	verge_4 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("verge_4"));
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> FoundMesh(TEXT("StaticMesh'/Game/MyContent/SupplyBase/SupplyBase.SupplyBase'"));//
	if (FoundMesh.Succeeded())
	{
		MyMesh->SetStaticMesh(FoundMesh.Object);
	}
	MyMesh->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);

	MyMesh->SetRelativeScale3D(FVector(0.5, 0.5, 0.5));
	MyMesh->SetRelativeRotation(FRotator(0, 180, 0));
	MyMesh->SetRelativeLocation(FVector(0, 0, 0));
	//////////////////////
	static ConstructorHelpers::FObjectFinder<UStaticMesh> FoundVerge(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));//
	if (FoundVerge.Succeeded())
	{
		verge_1->SetStaticMesh(FoundVerge.Object);
		verge_2->SetStaticMesh(FoundVerge.Object);
		verge_3->SetStaticMesh(FoundVerge.Object);
		verge_4->SetStaticMesh(FoundVerge.Object);
	}

	verge_1->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	verge_2->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	verge_3->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	verge_4->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);

	verge_1->SetRelativeScale3D(FVector(0.05, 3, 0.05));
	verge_2->SetRelativeScale3D(FVector(0.05, 3, 0.05));
	verge_3->SetRelativeScale3D(FVector(0.05, 3, 0.05));
	verge_4->SetRelativeScale3D(FVector(0.05, 3, 0.05));

	verge_1->SetRelativeLocation(FVector(150, 0, 0));
	verge_2->SetRelativeLocation(FVector(0, 150, 0));
	verge_3->SetRelativeLocation(FVector(-150, 0, 0));
	verge_4->SetRelativeLocation(FVector(0, -150, 0));

	verge_1->SetRelativeRotation(FRotator(0, 0, 0));
	verge_2->SetRelativeRotation(FRotator(0, 90, 0));
	verge_3->SetRelativeRotation(FRotator(0, 0, 0));
	verge_4->SetRelativeRotation(FRotator(0, 90, 0));

	static ConstructorHelpers::FObjectFinder<UMaterial> FoundMaterial(TEXT("/Game/MyContent/ForSomething.ForSomething")); //"Material'/Engine/EngineMaterials/CubeMaterial.CubeMaterial'"
	if (FoundMaterial.Succeeded())//Engine/VREditor/BasicMeshes/M_Floor_01.M_Floor_01
	{
		MyMaterial = FoundMaterial.Object;
	}

	MyDynamicMaterialInst = UMaterialInstanceDynamic::Create(MyMaterial, MyMesh);
	MyMesh->SetMaterial(0, MyDynamicMaterialInst);

	static ConstructorHelpers::FObjectFinder<UMaterial> FoundMaterialForVerge(TEXT("/Game/MyContent/ForFrame.ForFrame")); //"Material'/Engine/EngineMaterials/CubeMaterial.CubeMaterial'"
	if (FoundMaterialForVerge.Succeeded())//Engine/VREditor/BasicMeshes/M_Floor_01.M_Floor_01
	{
		MyMaterialForVerge = FoundMaterialForVerge.Object;
	}

	MyDynamicMaterialInstForVerge_1 = UMaterialInstanceDynamic::Create(MyMaterialForVerge, verge_1);
	verge_1->SetMaterial(0, MyDynamicMaterialInstForVerge_1);
	MyDynamicMaterialInstForVerge_2 = UMaterialInstanceDynamic::Create(MyMaterialForVerge, verge_2);
	verge_2->SetMaterial(0, MyDynamicMaterialInstForVerge_2);
	MyDynamicMaterialInstForVerge_3 = UMaterialInstanceDynamic::Create(MyMaterialForVerge, verge_3);
	verge_3->SetMaterial(0, MyDynamicMaterialInstForVerge_3);
	MyDynamicMaterialInstForVerge_4 = UMaterialInstanceDynamic::Create(MyMaterialForVerge, verge_4);
	verge_4->SetMaterial(0, MyDynamicMaterialInstForVerge_4);
}

// Called when the game starts or when spawned
void AMyResourceTower::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMyResourceTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyResourceTower::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}



