// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPawn.h"
#include "MyCamera.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/Core/Public/Containers/Queue.h"
#include "Runtime/Core/Public/Math/Vector.h"
#include "Runtime/Core/Public/Math/Rotator.h"
#include "SquareForMove.h"


// Sets default values
AMyPawn::AMyPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.

	//OnClicked.AddUniqueDynamic(this, &AMyCharacter::OnSelected);

	// Set this pawn to be controlled by the lowest-numbered player
	damage = 34;
	health = 100;
	psevdoHeaith = 100;
	inMove = false;
	inRotation = false;
	inRotationGun = false;

	MyGun = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Gun"));
	MyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	SpawnPlace = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SpawnPlace"));
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;
	//////////////////
	//////////////////

	static ConstructorHelpers::FObjectFinder<UStaticMesh> FoundMesh(TEXT("StaticMesh'/Game/MyContent/Tank_2/TankWithoutGun_1_.TankWithoutGun_1_'"));//"StaticMesh'/Engine/BasicShapes/Cube.Cube'"
	if (FoundMesh.Succeeded())
	{
		MyMesh->SetStaticMesh(FoundMesh.Object);
	}
	MyMesh->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);

	FVector vect = FVector(0.45, 0.45, 0.45);
	MyMesh->SetRelativeScale3D(vect);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> FoundMeshForGun(TEXT("StaticMesh'/Game/MyContent/Tank_2/TankGun_2_.TankGun_2_'"));//"StaticMesh'/Engine/BasicShapes/Cube.Cube'"
	if (FoundMeshForGun.Succeeded())
	{
		MyGun->SetStaticMesh(FoundMeshForGun.Object);
	}

	MyGun->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	MyGun->SetRelativeLocation(FVector(-13, 0, 37));
	MyGun->SetRelativeScale3D(FVector(0.5, 0.5, 0.5));


	SpawnPlace->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);

	SpawnPlace->SetRelativeLocation(FVector(-20, 0, 37));

	movement = 3;
	static ConstructorHelpers::FObjectFinder<UMaterial> FoundMaterial(TEXT("/Game/MyContent/ForSomething.ForSomething")); //"Material'/Engine/EngineMaterials/CubeMaterial.CubeMaterial'"
	if (FoundMaterial.Succeeded())//Engine/VREditor/BasicMeshes/M_Floor_01.M_Floor_01
	{
		MyMaterial = FoundMaterial.Object;
	}
	MyDynamicMaterialInst = UMaterialInstanceDynamic::Create(MyMaterial, MyMesh);
	MyMesh->SetMaterial(1, MyDynamicMaterialInst);

	forMove = 0;

	MyMesh->OnClicked.AddUniqueDynamic(this, &AMyPawn::OnClicked);
	direction = EDirectionEnum::VE_Down;
	

}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();
	Root->SetVisibility(false);
}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*if (inRotation == true) {
	if (rot == newRot) {

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, newRot.ToString());
	inRotation = false;
	inMove = true;
	forMove = 0;
	}
	forMove += 0.05;
	this->SetActorRotation(UKismetMathLibrary::RLerp(rot, newRot, forMove, true));
	if (this->GetActorRotation() == newRot || forMove >= 1) {

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, newRot.ToString());
	rot = newRot;
	inRotation = false;
	inMove = true;
	forMove = 0;
	}
	//UE_LOG(LogTemp, Warning, TEXT("!!"));
	}
	else if (inMove == true) {
	forMove += 0.05;
	this->SetActorLocation(UKismetMathLibrary::VLerp(loc, newLoc, forMove));
	if (this->GetActorLocation() == newLoc || forMove >= 1) {

	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, newLoc.ToString());
	loc = newLoc;
	forMove = 0;
	this->SetActorLocation(newLoc);
	__road.pop_front();
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::FromInt(__road.size()));
	if (__road.size() != 0) {
	newLoc = __road.front();//FVector(road.front()->x * 100 + 50, road.front()->y * 100 + 50, 20);
	inRotation = true;
	float x = this->GetActorLocation().X - (this->__road.front().X);
	if (x > 0) {
	this->newRot = FRotator(0, 0, 0);
	}
	else if (x < 0) {
	this->newRot = FRotator(0, 180, 0);
	}
	else {
	float y = this->GetActorLocation().Y - (this->__road.front().Y);
	if (y < 0) {
	this->newRot = FRotator(0, -90, 0);
	}
	else if (y > 0) {
	this->newRot = FRotator(0, 90, 0);
	}
	}
	}
	inMove = false;
	}
	//UE_LOG(LogTemp, Warning, TEXT("!"));
	}*/
	if (inRotation == true) {

		//myCamera->MyGameInterface->EndTurnButton->SetVisibility(ESlateVisibility::HitTestInvisible);
		if (rot == newRot) {
			inRotation = false;
			inMove = true;
			forMove = 0;
		}
		forMove += 0.05;

		if (forMove >= 1) {
			this->SetActorRotation(newRot);
			rot = newRot;
			inRotation = false;
			inMove = true;
			forMove = 0;
		}
		else {
			this->SetActorRotation(FMath::Lerp(rot, newRot, forMove));
		}
		//UE_LOG(LogTemp, Warning, TEXT("!!"));
	}
	else if (inMove == true) {

		//myCamera->MyGameInterface->EndTurnButton->SetVisibility(ESlateVisibility::HitTestInvisible);
		forMove += 0.05;

		if (forMove >= 1) {
			this->SetActorLocation(newLoc);
			loc = newLoc;
			forMove = 0;
			__road.pop_front();
			if (__road.size() != 0) {
				newLoc = __road.front();

				float x = this->GetActorLocation().X - (this->__road.front().X);
				if (x > 0) {
					this->newRot = FRotator(0, 0, 0);
				}
				else if (x < 0) {
					this->newRot = FRotator(0, 180, 0);
				}
				else {
					float y = this->GetActorLocation().Y - (this->__road.front().Y);
					if (y < 0) {
						this->newRot = FRotator(0, -90, 0);
					}
					else if (y > 0) {
						this->newRot = FRotator(0, 90, 0);
					}
				}
				inRotation = true;
			}
			else {

				myCamera->MyMain->inMove--;
			}
			inMove = false;
		}
		else {
			SetActorLocation(FMath::Lerp(loc, newLoc, forMove));
		}
	}
	else if (inRotationGun == true) {

		//myCamera->MyGameInterface->EndTurnButton->SetVisibility(ESlateVisibility::Collapsed);
		forMove += 0.05;

		if (forMove >= 1) {
			MyGun->SetWorldRotation(newRotForGun);
			if (newRotForGun.Roll < 1) {

				myCamera->MyMain->inMove--;
				//myCamera->MyGameInterface->EndTurnButton->SetVisibility(ESlateVisibility::Visible);
			}
			inRotationGun = false;
			forMove = 0;
		}
		else {
			MyGun->SetWorldRotation(FMath::Lerp(rotForGun, newRotForGun, forMove));
		}
	}
	if (tankProjectile != nullptr) {
		if (tankProjectile->inMove == false) {
			if (tankProjectile->target != nullptr) {
				tankProjectile->target->health -= tankProjectile->father->damage;
				if (tankProjectile->target->health <= 0) {
					if (myCamera->nameOfCube == tankProjectile->target) {
						if (myCamera->nameOfCube->arrayForMove.Num() != 0) {
							for (int i = myCamera->nameOfCube->arrayForMove.Num() - 1; i >= 0; i--) {
								if (myCamera->nameOfCube->arrayForMove[i] != nullptr) myCamera->nameOfCube->arrayForMove[i]->Destroy();
								myCamera->nameOfCube->arrayForMove.RemoveAt(i);
							}
						}

						if (myCamera->nameOfCube->arrayForAttack.Num() != 0) {
							for (int i = myCamera->nameOfCube->arrayForAttack.Num() - 1; i >= 0; i--) {
								if (myCamera->nameOfCube->arrayForAttack[i] != nullptr)  myCamera->nameOfCube->arrayForAttack[i]->Destroy();
								myCamera->nameOfCube->arrayForAttack.RemoveAt(i);
							}
						}

						while (myCamera->nameOfCube->__road.size() != 0) {
							myCamera->nameOfCube->__road.pop_front();
						}
						myCamera->nameOfCube = nullptr;
					}
					AMyPawn* tank = myCamera->MyMain->allTanks.FindAndRemoveChecked(tankProjectile->target->GetName());
					
					if (tank != nullptr) {
						int x = tank->GetActorLocation().X / 100;
						int y = tank->GetActorLocation().Y / 100;

						myCamera->world[x][y].empty = true;
						tank->Destroy();
						
					}

					myCamera->MyGameInterface->HorizontalBoxForTank->SetVisibility(ESlateVisibility::Collapsed);
					myCamera->MyGameInterface->AttackButton->SetVisibility(ESlateVisibility::Collapsed);
					myCamera->MyGameInterface->MoveButton->SetVisibility(ESlateVisibility::Collapsed);

				}
				else if (myCamera->nameOfCube == tankProjectile->target) {
					myCamera->MyGameInterface->HealthTextBlock->SetText(FText::FromString(FString::FromInt(tankProjectile->target->health)));
				}
				if (tankProjectile != nullptr) {
					tankProjectile->Destroy();
				}
				tankProjectile = nullptr;
				inRotationGun = true;
				rotForGun = MyGun->GetComponentRotation();
				newRotForGun = rotForGun;
				newRotForGun.Pitch = 0;
				forMove = 0;
			}
		}
	}
}

// Called to bind functionality to input
void AMyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AMyPawn::OnClicked(UPrimitiveComponent* Target, FKey ButtonPressed)
{
	if (team == Team::VE_Player) {
		if (this->inMove != true && this->inRotation != true) {
			myCamera->MyGameInterface->AttackButton->SetVisibility(ESlateVisibility::Visible);
			myCamera->MyGameInterface->MoveButton->SetVisibility(ESlateVisibility::Visible);
			myCamera->MyGameInterface->HorizontalBoxForTank->SetVisibility(ESlateVisibility::Visible);
			myCamera->MyGameInterface->HealthTextBlock->SetText(FText::FromString(FString::FromInt(this->health)));
			myCamera->MyGameInterface->MovementTextBlock->SetText(FText::FromString(FString::FromInt(this->movement)));
			myCamera->MyGameInterface->TankProductionButton->SetVisibility(ESlateVisibility::Collapsed);
			if (myCamera->nameOfCube != nullptr) {
				myCamera->nameOfCube->MyDynamicMaterialInst->SetScalarParameterValue("ColorAlpha", 0);
				if (myCamera->nameOfCube->arrayForMove.Num() != 0) {
					for (int i = myCamera->nameOfCube->arrayForMove.Num() - 1; i >= 0; i--) {
						if (myCamera->nameOfCube->arrayForMove[i] != nullptr) myCamera->nameOfCube->arrayForMove[i]->Destroy();
						myCamera->nameOfCube->arrayForMove.RemoveAt(i);
					}
				}

				if (myCamera->nameOfCube->arrayForAttack.Num() != 0) {
					for (int i = myCamera->nameOfCube->arrayForAttack.Num() - 1; i >= 0; i--) {
						if (myCamera->nameOfCube->arrayForAttack[i] != nullptr)  myCamera->nameOfCube->arrayForAttack[i]->Destroy();
						myCamera->nameOfCube->arrayForAttack.RemoveAt(i);
					}
				}

				while (myCamera->nameOfCube->__road.size() != 0) {
					myCamera->nameOfCube->__road.pop_front();
				}
			}
			myCamera->nameOfCube = this;
			MyDynamicMaterialInst->SetScalarParameterValue("ColorAlpha", 1);

			/////////////////////////////////////////////////////////////////////
			/*if (movement > 0) {
				for (AMyPawn* tank : myCamera->arrayOfCube) {
					if (this->GetDistanceTo(tank) <= 300) {
					FActorSpawnParameters SpawnInfo;
					SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
					FVector loc = tank->GetActorLocation();
					loc.Z = 100;

					APawnForAttack* pawnForAttack = GetWorld()->SpawnActor<APawnForAttack>(loc, FRotator(-180, 0, 0), SpawnInfo);
					pawnForAttack->father = this;
					pawnForAttack->attachedTank = tank;
					arrayForAttack.Add(pawnForAttack);
					}
				}
			}*/


			/////////////////////////////////////////////////////////////////////


			/*for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 20; j++) {
					if (myCamera->world[i][j].empty == false) {
						myCamera->world[i][j].passability = false;
					}
				}
			}
			//TQueue<ASquareForMove*> queue;
			FVector loc = this->GetActorLocation();
			loc.Z = 20;

			TQueue<ASquareForMove*> queue;

			int x = loc.X / 100;
			int y = loc.Y / 100;

			/////
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			FRotator Rotation(0, 0, 0);

			ASquareForMove* c = GetWorld()->SpawnActor<ASquareForMove>(loc, Rotation, SpawnInfo);
			c->Initialize(nullptr, myCamera->world[(int)x][(int)y], 0, (int)x, (int)y, this);
			queue.Enqueue(c);
			arrayForMove.Add(c);

			while (!queue.IsEmpty()) {
				ASquareForMove* cur;
				queue.Peek(cur);
				queue.Pop();
				if (cur != nullptr) {
					if (cur->x < 19 && cur->lenghtOfWay < movement) {
						if (myCamera != nullptr && myCamera->world[cur->x + 1][cur->y].passability == true) {
							myCamera->world[cur->x + 1][cur->y].passability = false;
							FVector l(cur->x * 100 + 150, cur->y * 100 + 50, 20);
							ASquareForMove* a = GetWorld()->SpawnActor<ASquareForMove>(l, Rotation, SpawnInfo);
							a->Initialize(cur, myCamera->world[cur->x + 1][cur->y], cur->lenghtOfWay + 1, cur->x + 1, cur->y, this);
							queue.Enqueue(a);
							arrayForMove.Add(a);
						}
					}
					if (cur->y < 19 && cur->lenghtOfWay < movement) {
						if (myCamera != nullptr && myCamera->world[cur->x][cur->y + 1].passability == true) {
							myCamera->world[cur->x][cur->y + 1].passability = false;
							FVector l(cur->x * 100 + 50, cur->y * 100 + 150, 20);
							ASquareForMove* a = GetWorld()->SpawnActor<ASquareForMove>(l, Rotation, SpawnInfo);
							a->Initialize(cur, myCamera->world[cur->x][cur->y + 1], cur->lenghtOfWay + 1, cur->x, cur->y + 1, this);
							queue.Enqueue(a);
							arrayForMove.Add(a);
						}
					}
					if (cur->y > 0 && cur->lenghtOfWay < movement) {
						if (myCamera != nullptr && myCamera->world[cur->x][cur->y - 1].passability == true) {
							myCamera->world[cur->x][cur->y - 1].passability = false;
							FVector l(cur->x * 100 + 50, cur->y * 100 - 50, 20);
							ASquareForMove* a = GetWorld()->SpawnActor<ASquareForMove>(l, Rotation, SpawnInfo);
							a->Initialize(cur, myCamera->world[cur->x][cur->y - 1], cur->lenghtOfWay + 1, cur->x, cur->y - 1, this);
							queue.Enqueue(a);
							arrayForMove.Add(a);
						}
					}
					if (cur->x > 0 && cur->lenghtOfWay < movement) {
						if (myCamera != nullptr && myCamera->world[cur->x - 1][cur->y].passability == true) {
							myCamera->world[cur->x - 1][cur->y].passability = false;
							FVector l(cur->x * 100 - 50, cur->y * 100 + 50, 20);
							ASquareForMove* a = GetWorld()->SpawnActor<ASquareForMove>(l, Rotation, SpawnInfo);
							a->Initialize(cur, myCamera->world[cur->x - 1][cur->y], cur->lenghtOfWay + 1, cur->x - 1, cur->y, this);
							queue.Enqueue(a);
							arrayForMove.Add(a);
						}
					}
				}
			}

			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 20; j++) {
					if (myCamera != nullptr) myCamera->world[i][j].passability = true;
				}
			}
			//*/
		}
	}

}