// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MyFactory.generated.h"

class SOMETHING_API AMyCamera;
enum class Team : uint8;

UCLASS()
class SOMETHING_API AMyFactory : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyFactory();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	Team team;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* MyMesh;

	UPROPERTY(VisibleAnywhere)
		UMaterial* MyMaterial;

	AMyCamera* myCamera;

	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* MyDynamicMaterialInst;

	UFUNCTION()
		void OnClicked(UPrimitiveComponent* Target, FKey ButtonPressed);
};