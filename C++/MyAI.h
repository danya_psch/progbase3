// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/Engine/Classes/Materials/Material.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Engine/StaticMeshActor.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectBaseUtility.h"
#include "MyGridWorld.h"
#include "MyFactory.h"
#include "Runtime/Core/Public/Containers/Map.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "GameInterface.h"
#include "MainPawn.h"
#include "MyResourceTower.h"
#include "MyPawn.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MyAI.generated.h"



UCLASS()
class SOMETHING_API AMyAI : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyAI();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void running();

	FVector CompStartLoc;

	void moveTank(AMyPawn* tank, int qq);

	AMyFactory* myFactory;

	TMap<FString, AMyPawn*> arrayOfTanks;

	AMainPawn* MyMain;

	int Money;
	
};
