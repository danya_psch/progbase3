// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "MyGridWorld.h"
#include <list>  
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SquareForMove.generated.h"

class SOMETHING_API AMyPawn;

UCLASS()
class SOMETHING_API ASquareForMove : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASquareForMove();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* MyMesh;

	UPROPERTY(VisibleAnywhere)
		UMaterial* MyMaterial;

	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* MyDynamicMaterialInst;

	MyGridWorld square;

	AMyPawn* father;

	UPROPERTY(VisibleAnywhere)
		ASquareForMove* prev;

	UPROPERTY(VisibleAnywhere)
		int lenghtOfWay;

	UPROPERTY(VisibleAnywhere)
		int x;

	UPROPERTY(VisibleAnywhere)
		int y;

	void Initialize(ASquareForMove* _prev, MyGridWorld _square, int _lenghtOfWay, int _x, int _y, AMyPawn* _father);

	UFUNCTION()
		void OnClicked(UPrimitiveComponent* Target, FKey ButtonPressed);

	UFUNCTION()
		void OnBeginCursorOver(UPrimitiveComponent* Target);

};
