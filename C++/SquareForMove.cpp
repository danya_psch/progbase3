// Fill out your copyright notice in the Description page of Project Settings.

#include "SquareForMove.h"
#include "MyPawn.h"
#include "MyCamera.h"
#include "Runtime/Core/Public/Containers/Array.h"
#include "UObject/ConstructorHelpers.h"



/*FMyWorld::FMyWorld() {
empty = true;
passability = true;
}*/

static void vectorChangeForGrid(FVector* vector);
static void vectorChangeFromGrid(FVector* vector);
// Sets default values
ASquareForMove::ASquareForMove()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = MyMesh;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> FoundMesh(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));
	if (FoundMesh.Succeeded())
	{
		MyMesh->SetStaticMesh(FoundMesh.Object);
	}

	FVector vect = FVector(1, 1, 0.001);
	MyMesh->SetRelativeScale3D(vect);

	static ConstructorHelpers::FObjectFinder<UMaterial> FoundMaterial(TEXT("/Game/MyContent/ForSquare.ForSquare")); //"Material'/Engine/EngineMaterials/CubeMaterial.CubeMaterial'"
	if (FoundMaterial.Succeeded())//Engine/VREditor/BasicMeshes/M_Floor_01.M_Floor_01
	{
		MyMaterial = FoundMaterial.Object;
	}
	MyDynamicMaterialInst = UMaterialInstanceDynamic::Create(MyMaterial, MyMesh);

	MyMesh->SetMaterial(0, MyDynamicMaterialInst);

	MyMesh->OnClicked.AddUniqueDynamic(this, &ASquareForMove::OnClicked);
	MyMesh->OnBeginCursorOver.AddUniqueDynamic(this, &ASquareForMove::OnBeginCursorOver);
}

void ASquareForMove::Initialize(ASquareForMove* _prev, MyGridWorld _square, int _lenghtOfWay, int _x, int _y, AMyPawn* _father) {
	this->prev = _prev;
	this->square = _square;
	this->lenghtOfWay = _lenghtOfWay;
	this->x = _x;
	this->y = _y;
	this->father = _father;
}

// Called when the game starts or when spawned
void ASquareForMove::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ASquareForMove::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FLinearColor colorVector = FLinearColor();
	father->myCamera->MyDynamicMaterialInst->GetVectorParameterValue("SelectedPos", colorVector);
	FVector loc = colorVector * 100;
	loc.Z = 20;
	if (loc == this->GetActorLocation()) {
		MyDynamicMaterialInst->SetScalarParameterValue("ColorAlpha", 1);
	}
	else {
		float i;
		MyDynamicMaterialInst->GetScalarParameterValue("ColorAlpha", i);
		if (i == 1) MyDynamicMaterialInst->SetScalarParameterValue("ColorAlpha", 0);
	}
}

// Called to bind functionality to input
void ASquareForMove::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}


void ASquareForMove::OnClicked(UPrimitiveComponent* Target, FKey ButtonPressed) {

	if (father->myCamera->nameOfCube != nullptr) {
		FVector loc = father->myCamera->nameOfCube->GetActorLocation();
		FLinearColor colorVector = FLinearColor();
		father->myCamera->MyDynamicMaterialInst->GetVectorParameterValue("SelectedPos", colorVector);
		FVector newLoc = colorVector;
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, newLoc.ToString());
		vectorChangeFromGrid(&newLoc);
		if (loc != newLoc && father->myCamera->world[(int)newLoc.X / 100][(int)newLoc.Y / 100].empty != false) {
			/*father->SetActorLocation(newLoc);
			loc /= 100;
			father->myCamera->world[(int)loc.X][(int)loc.Y].empty = true;
			father->myCamera->world[(int)newLoc.X / 100][(int)newLoc.Y / 100].empty = false;
			for (int i = father->arrayForMove.Num() - 1; i >= 0; i--) {
			if (father->arrayForMove[i] != nullptr) father->arrayForMove[i]->Destroy();
			father->arrayForMove.RemoveAt(i);
			}
			father->myCamera->nameOfCube->movement -= this->lenghtOfWay;
			father->myCamera->nameOfCube->MyDynamicMaterialInst->SetScalarParameterValue("ColorAlpha", 0);
			father->myCamera->nameOfCube = nullptr;*/


			////////////////////////////////////////////////

			father->myCamera->MyMain->inMove++;
			ASquareForMove* cur = this;
			while (cur->prev != nullptr) {
				father->__road.push_front(cur->GetActorLocation());
				cur = cur->prev;
			}
			father->mainLocation = father->__road.back();
			for (int i = father->arrayForMove.Num() - 1; i >= 0; i--) {
				if (father->arrayForMove[i] != nullptr) father->arrayForMove[i]->Destroy();
				father->arrayForMove.RemoveAt(i);
			}
			loc /= 100;
			father->myCamera->world[(int)loc.X][(int)loc.Y].empty = true;
			father->myCamera->world[(int)newLoc.X / 100][(int)newLoc.Y / 100].empty = false;
			/////////////
			float x = father->GetActorLocation().X - (father->__road.front().X);
			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::FromInt(x));
			if (x > 0) {
				father->newRot = FRotator(0, 0, 0);
			}
			else if (x < 0) {
				father->newRot = FRotator(0, 180, 0);
			}
			else {
				float y = father->GetActorLocation().Y - (father->__road.front().Y);
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::FromInt(y));
				if (y < 0) {
					father->newRot = FRotator(0, -90, 0);
				}
				else if (y > 0) {
					father->newRot = FRotator(0, 90, 0);
				}
			}
			father->rot = father->GetActorRotation();
			father->inRotation = true;
			/////////////
			//father->inMove = true;
			father->newLoc = father->__road.front();//FVector(father->myCamera->nameOfCube->road.front()->x * 100 + 50,father->myCamera->nameOfCube->road.front()->y * 100 + 50, 20);

			father->MyDynamicMaterialInst->SetScalarParameterValue("ColorAlpha", 0);
			father->movement -= this->lenghtOfWay;
			father->myCamera->MyGameInterface->AttackButton->SetVisibility(ESlateVisibility::Collapsed);
			father->myCamera->MyGameInterface->MoveButton->SetVisibility(ESlateVisibility::Collapsed);
			father->myCamera->nameOfCube = nullptr;


		}
	}
}

void ASquareForMove::OnBeginCursorOver(UPrimitiveComponent* Target) {

}

static void vectorChangeForGrid(FVector* vector) {
	vector->X /= 100;
	vector->Y /= 100;
	vector->X = floor(vector->X);
	vector->Y = floor(vector->Y);
	vector->X += 0.5;
	vector->Y += 0.5;
}

static void vectorChangeFromGrid(FVector* vector) {
	vector->X *= 100;
	vector->Y *= 100;
	vector->Z = 20;
}


