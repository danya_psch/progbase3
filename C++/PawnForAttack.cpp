// Fill out your copyright notice in the Description page of Project Settings.

#include "PawnForAttack.h"
#include "MyPawn.h"
#include "MyCamera.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "TankProjectile.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/TimelineComponent.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"


// Sets default values
APawnForAttack::APawnForAttack()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = MyMesh;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> FoundMesh(TEXT("StaticMesh'/Engine/BasicShapes/Cone.Cone'"));//"StaticMesh'/Engine/BasicShapes/Cube.Cube'"
	if (FoundMesh.Succeeded())
	{
		MyMesh->SetStaticMesh(FoundMesh.Object);
	}

	FVector vect = FVector(0.4, 0.4, 0.6);
	MyMesh->SetRelativeScale3D(vect);

	//////////////////////////////////
	inRotationForAttack = false;
	inRotationGun = false;
	static ConstructorHelpers::FObjectFinder<UMaterial> FoundMaterial(TEXT("/Game/MyContent/ForAttack.ForAttack")); //"Material'/Engine/EngineMaterials/CubeMaterial.CubeMaterial'"
	if (FoundMaterial.Succeeded())//Engine/VREditor/BasicMeshes/M_Floor_01.M_Floor_01
	{
		MyMaterial = FoundMaterial.Object;
	}
	MyDynamicMaterialInst = UMaterialInstanceDynamic::Create(MyMaterial, MyMesh);

	MyMesh->SetMaterial(0, MyDynamicMaterialInst);

	MyTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("TimeLine"));

	InterpFunction.BindUFunction(this, FName("TimeLineFloatReturn"));
	TimeLineFinished.BindUFunction(this, FName("OnTimeLineFinished"));

	static ConstructorHelpers::FObjectFinder<UCurveFloat> Curvy(TEXT("CurveFloat'/Game/MyContent/Curves/fCurve.fCurve'"));
	if (Curvy.Object != NULL) {
		fCurve = Curvy.Object;
	}

	MyMesh->OnClicked.AddUniqueDynamic(this, &APawnForAttack::OnClicked);
	ZOffset = 10.0f;
}

// Called when the game starts or when spawned
void APawnForAttack::BeginPlay()
{
	Super::BeginPlay();

	if (fCurve) {
		MyTimeline->AddInterpFloat(fCurve, InterpFunction, FName("Alpha"));
		MyTimeline->SetTimelineFinishedFunc(TimeLineFinished);

		StartLocation = GetActorLocation();

		EndLocation = FVector(StartLocation.X, StartLocation.Y, StartLocation.Z + ZOffset);

		MyTimeline->SetLooping(false);
		MyTimeline->SetIgnoreTimeDilation(true);

		MyTimeline->Play();
	}

}

// Called every frame
void APawnForAttack::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (inRotationForAttack == true) {
		forMove += 0.05;
		if (forMove >= 1 || rot == newRot) {
			father->SetActorRotation(newRot);
			rot = newRot;
			inRotationForAttack = false;
			forMove = 0;
			inRotationGun = true;
			FVector startLocation = father->SpawnPlace->GetComponentLocation();
			FVector endLocation = this->attachedTank->GetActorLocation();
			endLocation.Z = 57;
			float angle = FMath::Atan(160 / (FVector::Dist(startLocation, endLocation) / 2));
			rotForGun = father->MyGun->GetComponentRotation();
			newRotForGun = FRotator(-FMath::RadiansToDegrees(angle), 0, 0) + newRot;
		}
		else {
			this->father->SetActorRotation(FMath::Lerp(rot, newRot, forMove));
		}
	}
	else if (inRotationGun == true) {
		forMove += 0.05;
		if (forMove >= 1) {
			forMove = 0;
			inRotationGun = false;
			////////////////
			FActorSpawnParameters SpawnInfo;
			FVector startLocation = father->SpawnPlace->GetComponentLocation();
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			father->tankProjectile = GetWorld()->SpawnActor<ATankProjectile>(startLocation, FRotator(0, 0, 0), SpawnInfo);
			father->tankProjectile->father = father;
			father->tankProjectile->target = this->attachedTank;
			FVector endLocation = this->attachedTank->GetActorLocation();
			endLocation.Z = 160;
			father->tankProjectile->Initialize(startLocation, endLocation);
			for (int i = father->arrayForAttack.Num() - 1; i >= 0; i--) {
				if (father->arrayForAttack[i] != nullptr) father->arrayForAttack[i]->Destroy();
				father->arrayForAttack.RemoveAt(i);
			}
		}
		else {
			this->father->MyGun->SetWorldRotation(FMath::Lerp(rotForGun, newRotForGun, forMove));
		}
	}
}

// Called to bind functionality to input
void APawnForAttack::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APawnForAttack::TimeLineFloatReturn(float value)
{
	SetActorLocation(FMath::Lerp(StartLocation, EndLocation, value));
}

void APawnForAttack::OnTimeLineFinished()
{
	if (MyTimeline->GetPlaybackPosition() == 0.0f) {
		MyTimeline->Play();
	}
	else if (MyTimeline->GetPlaybackPosition() == MyTimeline->GetTimelineLength()) {
		MyTimeline->Reverse();
	}
}

void APawnForAttack::OnClicked(UPrimitiveComponent* Target, FKey ButtonPressed) {
	if (father != nullptr) {
		if (father->team != Team::VE_Computer) {

			if (inRotationForAttack != true && inRotationGun != true) {
				father->myCamera->MyMain->inMove++;
				father->movement -= 3;
				FRotator rotator = UKismetMathLibrary::FindLookAtRotation(father->GetActorLocation(), this->attachedTank->GetActorLocation()) - FRotator(0, 180, 0);
				newRot = rotator;
				rot = father->GetActorRotation();
				inRotationForAttack = true;
				father->myCamera->nameOfCube = nullptr;
				father->MyDynamicMaterialInst->SetScalarParameterValue("ColorAlpha", 0);
				father->myCamera->MyGameInterface->AttackButton->SetVisibility(ESlateVisibility::Collapsed);
				father->myCamera->MyGameInterface->MoveButton->SetVisibility(ESlateVisibility::Collapsed);
				for (int i = father->arrayForAttack.Num() - 1; i >= 0; i--) {
					if (father->arrayForAttack[i] != nullptr) father->arrayForAttack[i];
				}
				attachedTank->psevdoHeaith -= father->damage;
				if (attachedTank->psevdoHeaith <= 0) {
					//tank->myCamera->arrayOfTanks.FindAndRemoveChecked(_tank.Value->GetName());
					father->myCamera->MyMain->Ai->arrayOfTanks.Remove(attachedTank->GetName());
				}
				
				//father->myCamera->MyGameInterface->EndTurnButton->SetVisibility(ESlateVisibility::HitTestInvisible);

				/*father->SetActorRotation(rotator);
				FActorSpawnParameters SpawnInfo;
				FVector startLocation = father->SpawnPlace->GetComponentLocation();
				//FVector startLocation = father->GetActorLocation();
				SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				father->tankProjectile = GetWorld()->SpawnActor<ATankProjectile>(startLocation, FRotator(0, 0, 0), SpawnInfo);
				FVector endLocation = this->attachedTank->GetActorLocation();

				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, endLocation.ToString());
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, startLocation.ToString());
				endLocation.Z = 160;

				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, endLocation.ToString());
				father->tankProjectile->Initialize(startLocation, endLocation);*/
			}
		}
	}
}

