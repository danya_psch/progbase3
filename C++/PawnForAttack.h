// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/Engine/Classes/Components/TimelineComponent.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PawnForAttack.generated.h"

class SOMETHING_API AMyPawn;

UCLASS()
class SOMETHING_API APawnForAttack : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawnForAttack();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY()
		UTimelineComponent* MyTimeline;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere)
		UCurveFloat* fCurve;

	UPROPERTY()
		FVector StartLocation;

	UPROPERTY()
		FVector EndLocation;

	AMyPawn* father;

	AMyPawn* attachedTank;

	bool inRotationForAttack;

	bool inRotationGun;

	UPROPERTY()
		float ZOffset;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* MyMesh;

	UPROPERTY(VisibleAnywhere)
		UMaterial* MyMaterial;

	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* MyDynamicMaterialInst;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;

	FOnTimelineFloat InterpFunction{};

	FOnTimelineEvent TimeLineFinished{};

	FRotator rot;

	FRotator newRot;

	FRotator rotForGun;

	FRotator newRotForGun;

	float forMove;

	UFUNCTION()
		void TimeLineFloatReturn(float value);

	UFUNCTION()
		void OnTimeLineFinished();

	UFUNCTION()
		void OnClicked(UPrimitiveComponent* Target, FKey ButtonPressed);
};

