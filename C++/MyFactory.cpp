// Fill out your copyright notice in the Description page of Project Settings.

#include "MyFactory.h"
#include "MyCamera.h"
#include "UObject/ConstructorHelpers.h"


// Sets default values
AMyFactory::AMyFactory()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = MyMesh;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> FoundMesh(TEXT("StaticMesh'/Game/MyContent/Base_2/Base_2_.Base_2_'"));
	if (FoundMesh.Succeeded())
	{
		MyMesh->SetStaticMesh(FoundMesh.Object);
	}

	FVector vect = FVector(1.75, 1.75, 1.75);
	MyMesh->SetRelativeScale3D(vect);

	static ConstructorHelpers::FObjectFinder<UMaterial> FoundMaterial(TEXT("/Game/MyContent/ForSomething.ForSomething")); //"Material'/Engine/EngineMaterials/CubeMaterial.CubeMaterial'"
	if (FoundMaterial.Succeeded())//Engine/VREditor/BasicMeshes/M_Floor_01.M_Floor_01
	{
		MyMaterial = FoundMaterial.Object;
	}
	MyDynamicMaterialInst = UMaterialInstanceDynamic::Create(MyMaterial, MyMesh);


	MyMesh->SetMaterial(0, MyDynamicMaterialInst);
	MyMesh->OnClicked.AddUniqueDynamic(this, &AMyFactory::OnClicked);
}

// Called when the game starts or when spawned
void AMyFactory::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMyFactory::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyFactory::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void  AMyFactory::OnClicked(UPrimitiveComponent* Target, FKey ButtonPressed) {
	if (team == Team::VE_Player) {
		if (myCamera->nameOfCube != nullptr) {
			if (myCamera->nameOfCube != nullptr) {
				myCamera->nameOfCube->MyDynamicMaterialInst->SetScalarParameterValue("ColorAlpha", 0);
				if (myCamera->nameOfCube->arrayForMove.Num() != 0) {
					for (int i = myCamera->nameOfCube->arrayForMove.Num() - 1; i >= 0; i--) {
						if (myCamera->nameOfCube->arrayForMove[i] != nullptr) myCamera->nameOfCube->arrayForMove[i]->Destroy();
						myCamera->nameOfCube->arrayForMove.RemoveAt(i);
					}
				}

				if (myCamera->nameOfCube->arrayForAttack.Num() != 0) {
					for (int i = myCamera->nameOfCube->arrayForAttack.Num() - 1; i >= 0; i--) {
						if (myCamera->nameOfCube->arrayForAttack[i] != nullptr)  myCamera->nameOfCube->arrayForAttack[i]->Destroy();
						myCamera->nameOfCube->arrayForAttack.RemoveAt(i);
					}
				}

				while (myCamera->nameOfCube->__road.size() != 0) {
					myCamera->nameOfCube->__road.pop_front();
				}
			}
			myCamera->nameOfCube = nullptr;
		}
		myCamera->onFactory = true;
		myCamera->MyGameInterface->TankProductionButton->SetVisibility(ESlateVisibility::Visible);
		myCamera->MyGameInterface->HorizontalBoxForTank->SetVisibility(ESlateVisibility::Collapsed);
		myCamera->MyGameInterface->AttackButton->SetVisibility(ESlateVisibility::Collapsed);
		myCamera->MyGameInterface->MoveButton->SetVisibility(ESlateVisibility::Collapsed);
	}
	/*if (myCamera->world[0][0].empty == true) {
	myCamera->onFactory = true;
	myCamera->world[0][0].empty = false;
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	AMyPawn* cube = GetWorld()->SpawnActor<AMyPawn>(FVector(50, -50, 20), FRotator(0, -90, 0), SpawnInfo);
	cube->myCamera = myCamera;
	cube->loc = cube->GetActorLocation();
	cube->rot = cube->GetActorRotation();
	cube->__road.push_front(FVector(50, 50, 20));
	cube->newLoc = cube->__road.front();
	cube->inMove = true;
	myCamera->arrayOfCube.Add(cube);
	}*/
}



